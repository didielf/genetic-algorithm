import random, operator
class Fitness:
    def __init__(self, tour):
        self.tour = tour
        self.dist = 0
        self.f= 0.0
    
    def getFitness(self):
        if self.f == 0:
            self.f = 1 / float(self.getDistance())
        return self.f
    
    def getDistance(self):
        if self.dist == 0:
            rtn = 0
            for i in range(0, len(self.tour)):
                fromCity = self.tour[i]
                toCity = self.tour[0]
                if i + 1 < len(self.tour):
                    toCity = self.tour[i + 1]
                rtn += fromCity.distance(toCity)
            if rtn == 0:
                print("0 here --------------------------------------\n\n\n")
                print(self.tour)
            self.dist = rtn
        return self.dist


