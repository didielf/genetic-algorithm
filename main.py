# -*- coding: utf-8 -*-
"""
Created on Sun Oct 13 22:54:21 2019

@author: Didiel Fundora
"""

import operator, random as rnd, numpy, pandas
import matplotlib.pyplot as plt
import networkx as nx
from fitness import Fitness
from city import City

    

def rouletteWheelSelection(populationFitness, eliteSize):
    rtn = []
    dataFrame = pandas.DataFrame(numpy.array(populationFitness), columns=["id", "fitness"])
    dataFrame['cum_sum'] = dataFrame.fitness.cumsum()
    dataFrame['percent'] = 100*dataFrame.cum_sum/dataFrame.fitness.sum()
    pfl = len(populationFitness)
    lne = pfl - eliteSize

    for i in range(0, lne):
        rn = 100 * rnd.random()
        for i in range(0, pfl):
            if rn <= dataFrame.iat[i,3]:
                rtn.append(populationFitness[i][0])
                break
    return rtn

def tournamentSelection(populationFitness, eliteSize):
    rtn = []
    
    for i in range(0, len(populationFitness) - eliteSize):
        sampleSize = rnd.randrange(5, 20)
        popSample = rnd.sample(populationFitness, sampleSize)
        fittest = max(popSample, key=lambda x:x[1])
        rtn.append(fittest[0])
    return rtn
        
    

def selectPopulation(population, populationFitnes, eliteSize):
    rtn = []
    selection = []
    
    #rws = rouletteWheelSelection(populationFitnes, eliteSize)
    rws = tournamentSelection(populationFitnes, eliteSize)

    for i in range(0, eliteSize):
        selection.append(populationFitnes[i][0])
    
    selection = selection + rws
    
    for i in range(0, len(selection)):
        index = selection[i]
        rtn.append(population[index])
        
    return rtn

def crossoverPopulation(selectedPopulation, eliteSize):
    rtn = []
    spl = len(selectedPopulation)
    randomSample = rnd.sample(selectedPopulation, len(selectedPopulation))
    
    for i in range(0, eliteSize):
        rtn.append(selectedPopulation[i])
        
    n = spl - eliteSize
    for i in range(0, n):
        c = crossover(randomSample[i], randomSample[spl - i - 1])
        rtn.append(c)
    return rtn

def crossover(p1, p2):
    rtn = []
    section1 = []
    section2 = []
    r1 = int(rnd.random() * len(p1))
    r2 = int(rnd.random() * len(p2))
    
    start = min(r1,r2)
    end = max(r1,r2)
    
    for i in range(start, end):
        section1.append(p1[i])
    
    section2 = [item for item in p2 if item not in section1]
    rtn = section1 + section2
    return rtn


def mutatePopulation(pop, mutationRate):
    rtn = []
    for i in range(0, len(pop)):
        rtn.append(mutate(pop[i], mutationRate))
    return rtn

def mutate(indiv, mutationRate):
    for i in range(0, len(indiv)):
        if(rnd.random() < mutationRate):
            j = int(rnd.random() * len(indiv))
            item1 = indiv[i]
            item2 = indiv[j]
            indiv[i] = item2
            indiv[j] = item1
    return indiv
            
            

def getPopulationFitness(pop):
    rtn = {}
    for i in range(0, len(pop)):
        popFitness = Fitness(pop[i])
        rtn[i] = popFitness.getFitness()
    rtn = sorted(rtn.items(), 
                 key = operator.itemgetter(1), 
                 reverse = True)
    return rtn

#Creates the initial population
def createPopulation(size, cities):
    pop = []
    for i in range(0, size):
        tour = rnd.sample(cities, len(cities))
        pop.append(tour)
    return pop   

def getNextGeneration(currentPopulation, eliteSize, mutationRate):
    populationFitness = getPopulationFitness(currentPopulation)
    selectedPopulation = selectPopulation(currentPopulation, populationFitness, eliteSize)
    crossedOverPopulation = crossoverPopulation(selectedPopulation, eliteSize)
    mutatedPopulation = mutatePopulation(crossedOverPopulation, mutationRate)
    return mutatedPopulation


def runGenetic(cities, populationSize, mutationRate, eliteSize, generations):
    population = createPopulation(populationSize, cities)
    print("Initial Cost: " + str(1 / getPopulationFitness(population)[0][1])) 
    plotData = []
    for i in range(0, generations):
        population = getNextGeneration(population, eliteSize, mutationRate)
        plotData.append(1 / getPopulationFitness(population)[0][1])
    
    bestPopulation =  getPopulationFitness(population)[0]
    bestRoute = population[bestPopulation[0]]
    print("Final Cost: " + str(1 / bestPopulation[1]))
    
    plt.figure(3,figsize=(10,10)) 
    plt.plot(plotData, color='red')
    plt.grid(True)
    plt.ylabel('Cost', fontsize=14)
    plt.xlabel('Generation', fontsize=14)
    plt.show()
    
    
    g = nx.DiGraph()
    labels = {}
    color_map = []
    pathString = ''
    for i in range(0, len(bestRoute)):
        city = bestRoute[i]
        pathString += str(city.id) + ' -> '
        g.add_node(city.id, pos=(city.x, -city.y))
        labels[city.id] = city.id
        
        if (i > 0):
            fromCity = bestRoute[i-1]
            g.add_edge(fromCity.id, city.id)
            
        if(i == len(bestRoute) - 1):
            toCity = bestRoute[0]
            g.add_edge(city.id, toCity.id)
            pathString += str(toCity.id)

        if i == 0:
            color_map.append('#89e2a1')
        else: 
            color_map.append('#a2c5fd')
                     
    plt.figure(3,figsize=(14,14)) 
    
    pos = nx.get_node_attributes(g,'pos')
    nodes = nx.draw_networkx_nodes(g, pos, node_color=color_map)
    nodes.set_edgecolor('black')
    nx.draw_networkx_edges(g, pos, edge_color='gray')
    nx.draw_networkx_labels(g, pos, labels, node_size=140)
    
    #print path
    print(pathString)
        
    


cities = []
filepath = 'Random100.tsp'
with open(filepath) as fp:
   line = fp.readline()
   cnt = 1
   while line:
       if cnt > 7:
           cityInfo = line.strip().split(' ');
           cities.append(City(int(cityInfo[0]), float(cityInfo[1]), float(cityInfo[2])))
       line = fp.readline()
       cnt += 1

runGenetic(cities=cities, populationSize=200, mutationRate=0.001, eliteSize=20, generations=100)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    