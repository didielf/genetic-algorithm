import numpy

class City:
    def __init__(self, id, x, y):
        self.id = id
        self.x = x
        self.y = y
    
    def distance(self, city):
        distance = numpy.sqrt((abs(self.x - city.x) ** 2) + (abs(self.y - city.y) ** 2))
        return distance
    
    def __repr__(self):
        return str(self.id) + " " +  str(self.x) + " " + str(self.y)