# Genetic Algorithm for TSP

The genetic algorithm is an algorithm inspired by the process of natural selection that belongs to the larger class of evolutionary algorithms. The algorithm follows multiple steps in order to calculate the next population. Initially a set of cities are given with its respective x and y coordinates. Using the given cities, a random population is created by randomly selecting the order in which the salesman visit each city. The population contains k number of possible routes. After initializing the population, the fitness of each route in the population is determined. The fitness is determined by dividing 1 and the total distance of the route. Having the fitness for each route, we select the routes that will move to the next generation. There are two methods used to combine two routes to create a new route (Roulette wheel selection or tournament selection).

![Wheel](images/Roulette Wheel Selection Results.png)

![Wheel](images/Tournament Selection Results.png)
